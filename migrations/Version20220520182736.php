<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220520182736 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE viaje (id INT AUTO_INCREMENT NOT NULL, cod_viaje BIGINT NOT NULL, num_plazas DOUBLE PRECISION NOT NULL, origen VARCHAR(190) NOT NULL, destino VARCHAR(190) NOT NULL, precio DOUBLE PRECISION NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE viajero (id INT AUTO_INCREMENT NOT NULL, cedula VARCHAR(25) NOT NULL, nombre VARCHAR(80) NOT NULL, f_nacimiento DATE NOT NULL, telef VARCHAR(25) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE viaje');
        $this->addSql('DROP TABLE viajero');
    }
}
