import React, { useContext, useState, useEffect } from 'react';
import { TravellerContext } from '../../contexts/TravellerContext';
import { format, formatDistance, formatRelative, subDays } from 'date-fns'
import { useParams, useNavigate } from 'react-router-dom';
import axios from 'axios';



export function EditForm() {

    const context = useContext(TravellerContext);
    const { id } = useParams();
    const navigate = useNavigate();

    const [viajero, setViajero] = useState({
        cedula: '',
        nombre: '',
        f_nacimiento: '',
        telef: ''
    });

    const handleInputChange = (event) => {
        //console.log(event.target.name)
        // console.log(event.target.value)
        setViajero({
            ...viajero,
            [event.target.name]: event.target.value
        })
    }
    const enviarDatos = (event) => {
        event.preventDefault()
        console.log('enviando datos...' + viajero.cedula + ' ' + viajero.nombre)
    }


    const onUpdateSubmit = (event) => {
        event.preventDefault();
        context.updateTraveler(viajero.id, { id: viajero.id, cedula: viajero.cedula, f_nacimiento: viajero.f_nacimiento, nombre: viajero.nombre, telef: viajero.telef });
        navigate("/traveller-list")
    };

    useEffect(() => {
        axios.get('/api/viajero/' + id).then((response) => {
            setViajero(response.data);

        });
    }, []);


    if (!viajero) return "Sin Viajero data!"

    if (viajero.f_nacimiento) {
        const h = viajero.f_nacimiento
        const j = format(new Date(h), 'P');



    }

    return (
        <div className='container pt-4' >

            <div className="card">
                <div className="card-header" >

                    <div className="float-start">Editar Viajeros</div>

                </div>
                <div className="card-body">
                    <div className='container pt-4' >
                        <form className="row g-3" onSubmit={onUpdateSubmit}>
                            <div className="col-md-6">
                                <label htmlFor="cedula" className="form-label">Cedula</label>

                                <input name="cedula" type="text" className="form-control" id="cedula" value={viajero.cedula}
                                    onChange={handleInputChange} />
                            </div>
                            <div className="col-md-6">
                                <label htmlFor="f_nacimiento" className="form-label">Fecha de Nacimiento</label>
                                <input name="f_nacimiento" type="date" className="form-control" id="f_nacimiento" value={viajero.f_nacimiento}
                                    onChange={handleInputChange} pattern="\d{4}-\d{2}-\d{2}" />
                            </div>
                            <div className="col-12">
                                <label htmlFor="nombre" className="form-label">Nombre</label>
                                <input name="nombre" type="text" className="form-control" id="nombre" placeholder="Pedro Perez" value={viajero.nombre}
                                    onChange={handleInputChange} />
                            </div>
                            <div className="col-6">
                                <label htmlFor="telef" className="form-label">Telefono</label>
                                <input name="telef" type="phone" className="form-control" id="telef" placeholder="+58 412111111" value={viajero.telef}
                                    onChange={handleInputChange} />
                            </div>


                            <div className="col-12">
                                <button type="submit" className="btn btn-primary" >Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>

    );
};


