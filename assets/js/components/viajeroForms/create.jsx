import React, { useContext, useState } from 'react';
import { TravellerContext } from '../../contexts/TravellerContext';
import { compareAsc, format } from 'date-fns'




function CreateForm() {


    const context = useContext(TravellerContext);

    const [addCedula, setAddCedula] = useState('');
    const [addNacimiento, setAddNacimiento] = useState('');
    const [addNombre, setAddNombre] = useState('');
    const [addTelefono, setAddTelefono] = useState('');


    const onCreateSubmit = (event) => {
        event.preventDefault();
        context.createTraveler(event, { cedula: addCedula, f_nacimiento: addNacimiento, nombre: addNombre, telef: addTelefono });
        setAddCedula('');
        setAddNacimiento('');
        setAddNombre('');
        setAddTelefono('');
    };
     const myComponentStyle = {

        marginTop: '5rem !important',
        paddiningTop: '5rem'
     }


    return (
        <div className='container m-t-5'>
            <div className="card">
                <div className="card-header" style={myComponentStyle}>

                    <div className="float-start">Crear Pasajeros</div>

                </div>
                <div className="card-body">
                    <div className='container p-t-4' >
                        <form className="row g-3" onSubmit={onCreateSubmit}>
                            <div className="col-md-6">
                                <label htmlFor="cedula" className="form-label">Cedula</label>
                                <input name="cedula" type="text" className="form-control" id="cedula" value={addCedula}
                                    onChange={(event) => {
                                        setAddCedula(event.target.value);
                                    }} />
                            </div>
                            <div className="col-md-6">
                                <label htmlFor="f_nacimiento" className="form-label">Fecha de Nacimiento</label>
                                <input name="f_nacimiento" type="date" className="form-control" id="f_nacimiento" value={addNacimiento}
                                    onChange={(event) => {
                                        setAddNacimiento(event.target.value);
                                    }} pattern="\d{4}-\d{2}-\d{2}" />
                            </div>
                            <div className="col-12">
                                <label htmlFor="nombre" className="form-label">Nombre</label>
                                <input name="nombre" type="text" className="form-control" id="nombre" placeholder="Pedro Perez" value={addNombre}
                                    onChange={(event) => {
                                        setAddNombre(event.target.value);
                                    }} />
                            </div>
                            <div className="col-6">
                                <label htmlFor="telef" className="form-label">Telefono</label>
                                <input name="telef" type="phone" className="form-control" id="telef" placeholder="+58 412111111" value={addTelefono}
                                    onChange={(event) => {
                                        setAddTelefono(event.target.value);
                                    }} />
                            </div>


                            <div className="col-12">
                                <button type="submit" className="btn btn-primary" >Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    );
};



export default CreateForm;