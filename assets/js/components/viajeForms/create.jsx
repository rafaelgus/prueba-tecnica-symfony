import React, { useContext, useState } from 'react';
import { TravelContext } from '../../contexts/TravelContext';
import { compareAsc, format } from 'date-fns'




function CreateForm() {


    const context = useContext(TravelContext);

    const [addCodViaje, setAddCodViaje] = useState('');
    const [addNumPlazas, setAddNumPlazas] = useState('');
    const [addOrigen, setAddOrigen] = useState('');
    const [addDestino, setAddDestino] = useState('');
    const [addPrecio, setAddPrecio] = useState('');




    const onCreateSubmit = (event) => {
        event.preventDefault();
        context.createTravel(event, { cod_viaje: addCodViaje, num_plazas: addNumPlazas, origen: addOrigen, destino: addDestino, precio: addPrecio });
        setAddCodViaje('');
        setAddNumPlazas('');
        setAddOrigen('');
        setAddDestino('');
        setAddPrecio('');

    };
     

    return (
        <div className='container mt-5'>
            <div className="card">
                <div className="card-header" >

                    <div className="float-start">Crear Viajes</div>

                </div>
                <div className="card-body">
                    <div className='container pt-4' >
                        <form className="row g-3" onSubmit={onCreateSubmit}>
                            <div className="col-md-6">
                                <label htmlFor="cod_viaje" className="form-label">Codigo Viaje</label>
                                <input name="cod_viaje" type="text" className="form-control" id="cod_viaje" value={addCodViaje}
                                    onChange={(event) => {
                                        setAddCodViaje(event.target.value);
                                    }} />
                            </div>
                            <div className="col-md-6">
                                <label htmlFor="num_plazas" className="form-label">Numero de plazas</label>
                                <input name="num_plazas" type="text" className="form-control" id="num_plazas" value={addNumPlazas}
                                    onChange={(event) => {
                                        setAddNumPlazas(event.target.value);
                                    }}  />
                            </div>
                            <div className="col-md-6">
                                <label htmlFor="origen" className="form-label">Origen</label>
                                <input name="origen" type="text" className="form-control" id="origen" placeholder="Ciudad....." value={addOrigen}
                                    onChange={(event) => {
                                        setAddOrigen(event.target.value);
                                    }} />
                            </div>
                            <div className="col-md-6">
                                <label htmlFor="telef" className="form-label">Destino</label>
                                <input name="telef" type="text" className="form-control" id="telef" placeholder="Ciudad ...." value={addDestino}
                                    onChange={(event) => {
                                        setAddDestino(event.target.value);
                                    }} />
                            </div>
                            <div className="col-md-6">
                                <label htmlFor="precio" className="form-label">Precio</label>
                                <input name="precio" type="text" className="form-control" id="precio" placeholder="Precio ...." value={addPrecio}
                                    onChange={(event) => {
                                        setAddPrecio(event.target.value);
                                    }} />
                            </div>


                            <div className="col-12">
                                <button type="submit" className="btn btn-primary" >Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    );
};



export default CreateForm;