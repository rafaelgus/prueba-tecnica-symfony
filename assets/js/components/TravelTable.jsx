import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { TravelContext } from '../contexts/TravelContext';




function TravelTable() {


    const context = useContext(TravelContext);

    return (
        <div className='container pt-5' >
            <div className="card" >
                <div className="card-header">

                    <div className="float-start">Lista</div>

                    <div className="float-end">
                        <Link to="/travel/create" className="btn btn-success">

                            <i className="fa-solid fa-circle-plus"></i>
                            | Crear

                        </Link>
                    </div>

                </div>
                <div className="card-body">
                    <div className='container pt-5' >
                        <table className="table">
                            <thead>
                                <tr>
                                    <th>Cod. Viaje</th>
                                    <th>Num. Plazas</th>
                                    <th>Origen</th>
                                    <th>Destino</th>
                                    <th>Precio</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                {context.travels.map((travel, index) => (
                                    <tr key={'travel' + index}>
                                        <td>{travel.id}</td>
                                        <td>{travel.cod_viaje}</td>
                                        <td>{travel.num_plazas}</td>
                                        <td>{travel.origen}</td>
                                        <td>{travel.destino}</td>
                                        <td>{travel.precio}</td>
                                        <td>
                                            <div className="btn-group" role="group" aria-label="Basic mixed styles example">
                                                
                                                <button
                                                    type="button" className="btn btn-warning"
                                                    onClick={() => context.deleteTravel(travel.id)}
                                                >Eliminar</button>
                                            </div>
                                        </td>
                                    </tr>

                                ))}


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    )
};



export default TravelTable;