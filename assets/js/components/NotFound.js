//REACT
import React from 'react';
//MUI COMPONENTS

import { Link } from 'react-router-dom';

const NotFound = () => {
    return (


        <div className="card text-center">
            <div className="card-header">
                Not Found
            </div>
            <div className="card-body">
                <h5 className="card-title">Page not found 404</h5>

                <Link style={{ textDecoration: 'none' }} to="/">
                    <Button color="primary" variant="contained" size="large">Go back the the
                        homepage</Button>
                </Link>

            </div>
            <div className="card-footer text-muted">
                2 days ago
            </div>
        </div>
    );
};

export default NotFound;