import React, { useContext, useState, useEffect } from 'react';
import { TravellerContext } from '../../contexts/TravellerContext';
import { format, formatDistance, formatRelative, subDays } from 'date-fns'
import { useParams, useNavigate } from 'react-router-dom';
import axios from 'axios';



export function TablaViajes() {
    const context = useContext(TravellerContext);
    const navigate = useNavigate();
    const { id } = useParams();

    const [viajes, setViajes] = useState();
    const [viajero, setViajero] = useState();

   
    const enviarDatos = (viajero, viaje) => {
        //event.preventDefault()
        console.log('enviando datos...' + viajero + ' ' + viaje)
    }


    const onAddSubmit = (viajero, viaje) => {
        
        context.addTravel(viajero, viaje);
       
    };

    useEffect(() => {
        axios.get('/api/viajero/' + id).then((response) => {
            setViajero(response.data);

        });

        axios.get('/api/viajes')
            .then(response => {
                setViajes(response.data)
            }).catch(error => {
                console.log(error);
            });
        
    }, []);


   console.log(viajero);
   
    if (!viajes) return "En espera"
    return (
        <div className='container pt-5' >
            <div className="card" >
                <div className="card-header">

                    <div className="float-start">Viajes disponibles</div>

                </div>
                <div className="card-body">
                    <div className='container pt-5' >
                        <table className="table">
                            <thead>
                                <tr>
                                    <th>Cod. Viaje</th>
                                    <th>Num. Plazas</th>
                                    <th>Origen</th>
                                    <th>Destino</th>
                                    <th>Precio</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                {viajes.map((viaje, index) => (
                                    <tr key={'viaje' + index}>
                                        
                                        <td>{viaje.cod_viaje}</td>
                                        <td>{viaje.num_plazas}</td>
                                        <td>{viaje.origen}</td>
                                        <td>{viaje.destino}</td>
                                        <td>{viaje.precio}</td>
                                        <td>
                                            <div className="btn-group" role="group" aria-label="Basic mixed styles example">
                                                
                                            <button
                                                    type="button" className="btn btn-warning"
                                                    onClick={() => onAddSubmit(viajero.id, viaje.id)}
                                                >Asignar</button>
                                            </div>
                                        </td>
                                    </tr>

                                ))}


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    );
};


