//REACT
import React, { useState } from 'react';

import { Link } from 'react-router-dom';


const Navigation = () => {


    return (
        <div className="container pb-5">
            <nav className="navbar navbar-expand-lg bg-light">
                <div className="container-fluid">
                  
                    <Link className="navbar-brand" to="/traveller-list">
                        Ir A Viajeros
                    </Link>
                    <Link className="navbar-brand" to="/travel-list">
                        ir a Viajes
                    </Link>
                </div>
            </nav>
        </div>

        
    );
};

export default Navigation;