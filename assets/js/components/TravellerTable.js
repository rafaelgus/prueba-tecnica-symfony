import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { TravellerContext } from '../contexts/TravellerContext';
import { compareAsc, format } from 'date-fns'




function TravellerTable() {


    const context = useContext(TravellerContext);
    return (
        <div className='container'>
            <div className="card">
                <div className="card-header">

                    <div className="float-start">Listado de Pasajeros</div>

                    <div className="float-end">
                        <Link to="/traveller/create" className="btn btn-success">

                            <i className="fa-solid fa-circle-plus"></i>
                            | Crear

                        </Link>

                    </div>

                </div>
                <div className="card-body">
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Cedula</th>
                                <th>Nombre</th>
                                <th>Nacimiento</th>
                                <th>Telefono</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {context.travellers.map((traveller, index) => (
                                <tr key={'traveller' + index}>
                                    <td>{traveller.cedula}</td>
                                    <td>{traveller.nombre}</td>
                                    <td>{traveller.f_nacimiento}</td>

                                    <td>{traveller.telef}</td>
                                    <td>
                                        <div className="btn-group" role="group" aria-label="Basic mixed styles example">
                                            
                                            <Link to={"/traveller/edit/" + traveller.id }className="btn btn-danger">
                                                
                                                 Editar
                                            </Link>
                                            <button 
                                            type="button" className="btn btn-warning"
                                            onClick={()=>context.deleteTraveler(traveller.id)}
                                            >Eliminar</button>
                                           
                                        </div>
                                    </td>
                                </tr>

                            ))}


                        </tbody>
                    </table>

                </div>
            </div>
        </div>


    )
};



export default TravellerTable;