
//REACT
import React from 'react';
//ROUTER


import { BrowserRouter, Navigate, Route, Routes, Link } from "react-router-dom";

//CUSTOM COMPONENTS
import NotFound from './NotFound';
import Navigation from './Navigation';
import Viajeros from '../pages/Viajeros';
import Viajes from '../pages/Viaje';
import Home from '../pages/home';
import ViajeroCreate from '../pages/ViajeroLogic/create';
import ViajeCreate from '../pages/ViajeLogic/create';
import EditViajero from '../pages/ViajeroLogic/edit';




const Router = () => {

    return (
        <div>
            <Navigation />

            <Routes>
                <Route path="/" element={<Home />} />

                <Route exact path="/traveller-list" element={<Viajeros />} />
                <Route exact path="/traveller/create" element={<ViajeroCreate />} />
                <Route exact path="/traveller/edit/:id" element={<EditViajero />} />

                <Route exact path="/travel-list" element={<Viajes />} />
                <Route exact path="/travel/create" element={<ViajeCreate />} />
                <Route component={NotFound} />

            </Routes>
        </div>


    );
};

export default Router;