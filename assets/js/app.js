
import React from 'react';
import ReactDom from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { createRoot } from 'react-dom/client';


import Router from './components/Router';

class App extends React.Component {

    render() {
        return <Router />;
    }
}

const container = document.getElementById('root');
const root = createRoot(container); 

root.render( 
<BrowserRouter>
    <App />
</BrowserRouter>);

/* ReactDom.render(
    <BrowserRouter>
        <App />
    </BrowserRouter>, document.getElementById('root')); */