
import React, { useContext }  from 'react';
import ReactDom from 'react-dom';


import TravellerTable from '../components/TravellerTable';
import TravellerContextProvider from '../contexts/TravellerContext';



class Viajeros extends React.Component {


    render() {
        return (
            <TravellerContextProvider>
                <TravellerTable />
            </TravellerContextProvider>
        )

    }
}


export default Viajeros;