
import React from 'react';



import TravelTable from '../components/TravelTable';
import TravelContextProvider from '../contexts/TravelContext';



class Viajes extends React.Component {


    render() {
        return (
            <TravelContextProvider>
                <TravelTable />
            </TravelContextProvider>
        )

    }
}


export default Viajes;