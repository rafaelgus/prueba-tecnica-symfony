
import React, { useContext }  from 'react';
import ReactDom from 'react-dom';
import CreateForm from '../../components/viajeroForms/create';



import TravellerContextProvider from '../../contexts/TravellerContext';



class ViajeroCreate extends React.Component {


    render() {
        return (
            <TravellerContextProvider>
                <CreateForm/>
            </TravellerContextProvider>
        )

    }
}


export default ViajeroCreate;