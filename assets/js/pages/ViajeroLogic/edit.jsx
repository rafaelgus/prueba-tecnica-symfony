import React, { Component } from 'react';
import { EditForm } from '../../components/viajeroForms/edit';
import { TablaViajes } from '../../components/ViajeroTablas/ViajesDisponibles';
import TravellerContextProvider from '../../contexts/TravellerContext';

class EditViajero extends Component {
    
  
    render() {
        return (
            <TravellerContextProvider>
                <EditForm/>
                <TablaViajes/>
            </TravellerContextProvider>
        )

    }
    
}
 
export default EditViajero;