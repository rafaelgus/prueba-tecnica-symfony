
import React, { useContext }  from 'react';
import ReactDom from 'react-dom';
import CreateForm from '../../components/viajeForms/create';



import TravelContextProvider from '../../contexts/TravelContext';



class ViajeCreate extends React.Component {


    render() {
        return (
            <TravelContextProvider>
                <CreateForm/>
            </TravelContextProvider>
        )

    }
}


export default ViajeCreate;