import React from 'react';
import {
    createContext
} from 'react';
import axios from 'axios';

export const TravelContext = createContext();

class TravelContextProvider extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            travels: []
        };

        this.listTravels();

    }

    listTravels() {
        axios.get('/api/viajes')
            .then(response => {
                this.setState({
                    travels: response.data,
                })
            }).catch(error => {
                console.log(error);
            });


    }
    //create
    createTravel(event, travel) {
        event.preventDefault();
        axios.post('/api/viaje/create', travel)
            .then(response => {
                console.log(response);
                this.setState({
                    travels: response.data,
                    message: response.data.message,
                });

            }).catch(error => {
                console.error(error);
            });

    }

    //update
    updateTravel(id, travel) {
        axios.put('/api/viaje/update/' + id, travel)
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                console.error(error);
            });
    }



    //delete
    deleteTravel(id) {
        axios.delete('/api/viaje/delete/' + id)
            .then(response => {
                console.log(response);
                this.listTravels()
            }).catch(error => {
                console.log(error);
            });
    }

    render() {
        return ( 
            <TravelContext.Provider value = {
                {
                    ...this.state,
                    createTravel: this.createTravel.bind(this),
                    updateTravel: this.updateTravel.bind(this),
                    deleteTravel: this.deleteTravel.bind(this)
                }
            } > {
                this.props.children
            } 
            </TravelContext.Provider>
        )
    }
}

export default TravelContextProvider;