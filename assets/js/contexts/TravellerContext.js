import React from 'react';
import {
    createContext
} from 'react';
import axios from 'axios';

export const TravellerContext = createContext();

class TravellerContextProvider extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            travellers: [],
            traveler: '',
        };

        this.listTravelers();

    }



    listTravelers() {
        axios.get('/api/viajeros')
            .then(response => {

                this.setState({
                    travellers: response.data,
                })
            }).catch(error => {
                console.log(error);
            });
    }


    //create
    createTraveler(event, traveler) {
        event.preventDefault();
        axios.post('/api/viajero/create', traveler)
            .then(response => {

                this.setState({
                    travellers: response.data,
                    message: response.data.message,
                });

            }).catch(error => {
                console.error(error);
            });

    }

    //update
    updateTraveler(id, traveler) {

        axios.put('/api/viajero/update/' + id, traveler)
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                console.error(error);
            });
    }


    addTravel(userId, viajeId ) {

        axios.post('/api/viajero/add/' + userId +'/viaje/'+ viajeId)
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                console.error(error);
            });
    }



    //delete
    deleteTraveler(id) {
        axios.delete('/api/viajero/delete/' + id)
            .then(response => {

                console.log(response);
                this.listTravelers()
            }).catch(error => {
                console.log(error);
            });
    }

    render() {
        return ( 
            < TravellerContext.Provider value = {
                {
                    ...this.state,
                    createTraveler: this.createTraveler.bind(this),
                    updateTraveler: this.updateTraveler.bind(this),
                    deleteTraveler: this.deleteTraveler.bind(this),
                    addTravel: this.addTravel.bind(this)



                }
            } > {
                this.props.children
            } 
            </TravellerContext.Provider>
        )
    }
}

export default TravellerContextProvider;