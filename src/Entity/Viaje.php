<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\ViajeRepository;
use Doctrine\Common\Collections\ArrayCollection;

#[ORM\Entity(repositoryClass: ViajeRepository::class)]
class Viaje
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'bigint')]
    private $cod_viaje;

    #[ORM\Column(type: 'float')]
    private $num_plazas;

    #[ORM\Column(type: 'string', length: 190)]
    private $origen;

    #[ORM\Column(type: 'string', length: 190)]
    private $destino;

    #[ORM\Column(type: 'float')]
    private $precio;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $created_at;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $updated_at;

    #[ORM\ManyToMany(targetEntity: Viajero::class, mappedBy: 'viajes')]
    private $viajeros;

    

    public function __construct()
    {
        $this->viajeros = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodViaje(): ?string
    {
        return $this->cod_viaje;
    }

    public function setCodViaje(string $cod_viaje): self
    {
        $this->cod_viaje = $cod_viaje;

        return $this;
    }

    public function getNumPlazas(): ?float
    {
        return $this->num_plazas;
    }

    public function setNumPlazas(float $num_plazas): self
    {
        $this->num_plazas = $num_plazas;

        return $this;
    }

    public function getOrigen(): ?string
    {
        return $this->origen;
    }

    public function setOrigen(string $origen): self
    {
        $this->origen = $origen;

        return $this;
    }

    public function getDestino(): ?string
    {
        return $this->destino;
    }

    public function setDestino(string $destino): self
    {
        $this->destino = $destino;

        return $this;
    }

    public function getPrecio(): ?float
    {
        return $this->precio;
    }

    public function setPrecio(float $precio): self
    {
        $this->precio = $precio;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection<int, Viajero>
     */
    public function getViajeros(): Collection
    {
        return $this->viajeros;
    }

    public function addViajero(Viajero $viajero): self
    {
        if (!$this->viajeros->contains($viajero)) {
            $this->viajeros[] = $viajero;
            $viajero->addViaje($this);
        }

        return $this;
    }

    public function removeViajero(Viajero $viajero): self
    {
        if ($this->viajeros->removeElement($viajero)) {
            $viajero->removeViaje($this);
        }

        return $this;
    }
}
