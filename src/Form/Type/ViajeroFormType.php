<?php

namespace App\Form\Type;

use App\Entity\Viajero;
use App\Form\Model\ViajeroDto;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ViajeroFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cedula', TextType::class)
            ->add('nombre', TextType::class)
            ->add('f_nacimiento', DateType::class,  [
                'widget'=>'single_text',
                'format' => 'yyyy-MM-dd',
                'html5' => false
            ])
            ->add('telef', TextType::class)
            ->add('viajes',  CollectionType::class, [
                'allow_add' => true,
                 'allow_delete' => true,
                 'entry_type' => ViajeFormType::class,   
            ]);
           
            
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Viajero::class,
            'csrf_protection' => false,
            
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }

    public function getName()
    {
        return '';
    }
}