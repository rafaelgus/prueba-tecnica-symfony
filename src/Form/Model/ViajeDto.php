<?php

namespace App\Form\Model;

class ViajeDto
{

    public $cod_viaje;
    public $num_plazas;
    public $origen;
    public $destino;
    public $precio;
}
