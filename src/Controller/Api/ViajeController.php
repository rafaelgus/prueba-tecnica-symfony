<?php

namespace App\Controller\Api;

use Exception;
use Throwable;
use App\Entity\Viaje;
use App\Form\Model\ViajeDto;
use FOS\RestBundle\View\View;
use App\Form\Type\ViajeFormType;
use App\Repository\ViajeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\View as ViewAttribute;
use FOS\RestBundle\Controller\Annotations\{Delete, Get, Post, Put, Patch};
use DateTime;





class ViajeController extends AbstractFOSRestController
{

    #@OA\Response(
    #response=200,
    #@Model(type=Viaje::class, groups={"non_sensitive_data"})
    #)
    #@OA\Tag(name="Viaje")
    #[Get(path: "/api/viajes")]
    #[ViewAttribute(serializerGroups: ['viaje'], serializerEnableMaxDepthChecks: true)]

    public function getAction(
        ViajeRepository $viajeRepository
    ) {

        return  $viajeRepository->findAll();
    }



    /***
     * @OA\Response(
     *  response=200,
     *   @Model(type=Viaje::class)
     *  )
     *  @OA\Parameter(
     *  name="id",
     * )      
     * 
     */
    #[Get(path: "/api/viaje/{id}")]
    #[ViewAttribute(serializerGroups: ['viaje'], serializerEnableMaxDepthChecks: true)]

    public function findByIdAction(
        ViajeRepository $viajeRepository,
        Viaje $viaje
    ) {

        return  $viajeRepository->find($viaje);
    }


    /***
     * @OA\Response(
     *  response=200,
     *   @Model(type=Viaje::class)
     *  )
     *  
     */
    #[Post(path: "/api/viaje/create")]
    #[ViewAttribute(serializerGroups: ['viaje'], serializerEnableMaxDepthChecks: true)]
    public function postAction(
        EntityManagerInterface $em,
        Request $request

    ) {

        //dd(json_decode($request->getContent()));

        try {

            $viajeDto = new Viaje();
            $form = $this->createForm(ViajeFormType::class, $viajeDto);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em->persist($viajeDto);

                try {
                    $em->flush();
                } catch (\Throwable $th) {
                    //throw $th;
                    return $this->json([
                        'message' => $th,
                        'status' => 500
                    ]);
                }

                return $this->json([
                    'message' => 'el viajero ha sido creado con exito',
                    'status' => 200
                ]);

                return $viajeDto;
            }
        } catch (\Throwable $th) {
            return $this->json([
                'message' => $th,
                'status' => 500
            ]);
        }




        return $this->json([
            'message' => 'Ha Ocurrido un error',
            'status' => 500
        ]);
    }


    /***
     * @OA\Response(
     *  response=200,
     *   @Model(type=Viaje::class)
     *  )
     *  @OA\Parameter(
     *  name="id",
     * )      
     */

    #[Put(path: "/api/viaje/update/{id}")]
    #[ViewAttribute(serializerGroups: ['viaje'], serializerEnableMaxDepthChecks: true)]
    public function updateAction(
        EntityManagerInterface $em,
        Request $request,
        Viaje $viaje

    ) {
        $contenido = json_decode($request->getContent());



        $viaje->setCodViaje($contenido->cod_viaje);
        $viaje->setNumPlazas($contenido->num_plazas);
        $viaje->setOrigen($contenido->origen);
        $viaje->setDestino($contenido->destino);
        $viaje->setPrecio($contenido->precio);


        try {
            $em->flush();
        } catch (\Throwable $th) {
            //throw $th;
            return $this->json([
                'message' => $th,
                'status' => 500
            ]);
        }

        return $this->json([
            'message' => 'el viaje ha sido actualizado con exito',
            'status' => 200
        ]);
    }


    /***
     * @OA\Response(
     *  response=200,
     *   @Model(type=Viaje::class)
     *  )
     *  @OA\Parameter(
     *  name="id",
     * )      
     */

    #[Delete(path: "/api/viaje/delete/{id}")]
    #[ViewAttribute(serializerGroups: ['viaje'], serializerEnableMaxDepthChecks: true)]

    public function removeAction(
        ViajeRepository $viajeRepository,
        Viaje $viaje
    ) {
        //dd($viajero);

        return  $viajeRepository->remove($viaje, $flush = true);
    }
}
