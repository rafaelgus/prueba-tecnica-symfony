<?php

namespace App\Controller\Api;

use App\Entity\Viaje;
use App\Entity\Viajero;
use App\Form\Type\ViajeroFormType;
use App\Repository\ViajeroRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\View as ViewAttribute;
use FOS\RestBundle\Controller\Annotations\{Delete, Get, Post, Put, Patch};



class ViajeroController extends AbstractController
{

    #@OA\Response(
    #response=200,
    #@Model(type=Viajero::class, groups={"non_sensitive_data"})
    #)
    #@OA\Tag(name="Viajeros")
    #[Get(path: "/api/viajeros")]
    #[ViewAttribute(serializerGroups: ['viajero'], serializerEnableMaxDepthChecks: true)]

    public function getAction(
        ViajeroRepository $viajeroRepository
    ) {

        return  $viajeroRepository->findAll();
    }

    #[Get(path: "/api/viajero/{id}")]
    #[ViewAttribute(serializerGroups: ['viajero'], serializerEnableMaxDepthChecks: true)]

    public function findByIdAction(
        ViajeroRepository $viajeroRepository,
        Viajero $viajero
    ) {

        return  $viajeroRepository->find($viajero);
    }


    #[Post(path: "/api/viajero/create")]
    #[ViewAttribute(serializerGroups: ['viajero'], serializerEnableMaxDepthChecks: true)]
    public function postAction(
        EntityManagerInterface $em,
        Request $request

    ) {
        //dd($request);
        $viajeroDto = new Viajero();

        $form = $this->createForm(ViajeroFormType::class, $viajeroDto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($viajeroDto);
            $em->flush();

            return $viajeroDto;
        }

        return $form;
    }


    #[Put(path: "/api/viajero/update/{id}")]
    #[ViewAttribute(serializerGroups: ['viajero'], serializerEnableMaxDepthChecks: true)]
    public function updateAction(
        EntityManagerInterface $em,
        Request $request,
        Viajero $viajero

    ) {
        $contenido = json_decode($request->getContent());

        $date = new DateTime($contenido->f_nacimiento);
        
       

        $viajero->setNombre($contenido->nombre);
        $viajero->setCedula($contenido->cedula);
        $viajero->setFNacimiento($date);
        $viajero->setTelef($contenido->telef);

        try {
            $em->flush();

        } catch (\Throwable $th) {
            //throw $th;
            return $this->json([
                'message' => $th,
                'status' => 500
            ]);
        }

        return $this->json([
            'message' => 'el viajero ha sido actualizado con exito',
            'status'=> 200
        ]);
        
    }

    #[Post(path: "/api/viajero/add/{userId}/viaje/{viajeId}")]
    #[ViewAttribute(serializerGroups: ['viajero'], serializerEnableMaxDepthChecks: true)]
    public function addTravelAction(
        EntityManagerInterface $em,
        Request $request,
        
       

    ) {
        $viajero = $em->getRepository(Viajero::class)->find($request->attributes->get('userId'));
        $viaje = $em->getRepository(Viaje::class)->find($request->attributes->get('viajeId'));
       

        


        try {

            $viajero->addViaje($viaje);
            $viaje->addViajero($viajero);

            $em->persist($viajero);
            $em->flush();

        } catch (\Throwable $th) {
            //throw $th;
            return $this->json([
                'message' => $th,
                'status' => 500
            ]);
        }

        return $this->json([
            'message' => 'el viaje ha sido agregado con exito',
            'status'=> 200
        ]);
        
    }

    #[Delete(path: "/api/viajero/delete/{id}")]
    #[ViewAttribute(serializerGroups: ['viajero'], serializerEnableMaxDepthChecks: true)]

    public function removeAction(
        ViajeroRepository $viajeroRepository,
        Viajero $viajero
    ) {
        //dd($viajero);

        return  $viajeroRepository->remove($viajero, $flush = true);
    }

}
