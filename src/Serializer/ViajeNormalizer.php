<?php

namespace App\Serializer;


use App\Entity\Viaje;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;


class ViajeNormalizer implements NormalizerInterface
{
    private $normalizer;

    public function __construct(
        ObjectNormalizer $normalizer,
        private UrlHelper $urlHelper
    ) {
        $this->normalizer = $normalizer;
    }

    public function normalize($viaje, $format = null, array $context = [])
    {
        $data = $this->normalizer->normalize($viaje, $format, $context);
        
        
        return $data;
    }

    public function supportsNormalization($data, $format = null, array $context = [])
    {
        return $data instanceof Viaje;
    }
}