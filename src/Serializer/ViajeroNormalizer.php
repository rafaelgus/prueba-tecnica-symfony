<?php

namespace App\Serializer;


use App\Entity\Viaje;
use App\Entity\Viajero;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;


class ViajeroNormalizer implements NormalizerInterface
{
  

    public function __construct(
        private ObjectNormalizer $normalizer,
        private UrlHelper $urlHelper
    ) {
       
    }

    public function normalize($viajero, $format = null, array $context = [])
    {
        $data = $this->normalizer->normalize($viajero, $format, $context);
        
        
        return $data;
    }

    public function supportsNormalization($data, $format = null, array $context = [])
    {
        return $data instanceof Viajero;
    }
}