# React-Symfony
Prueba técnica - React - Symfony - PHP  - Boostrap - MariaDB

# Backend en symfony:
-Este backend estructurado con dos entidades relacionadas de muchos a muchos, donde se va guardar la información necesaria para darle servicios a nuestros frontend.
# 1) Rutas de uso:
  
 - Documentacion de los end Points de la api: Route('/api/doc’)
 - Guardar un Viajero: Route('/api/viajero/create’)
 - Listar Viajeros: Route('/api/viajeros/')
 - Consultar Viajeros: Route('/api/viajeros/')
 - Actualizar Viajero: Route('/api/viajero/update/{id}')
 - Eliminar Viajero: Route('/api/viajero/delete/{id}')
 - Guardar un Viaje: Route('/api/viajero/create’)
 - Listar Viaje:     Route('/api/viajeros/')
 - Consultar Viaje:  Route('/api/viajeros/')
 - Actualizar Viaje: Route('/api/viajero/update/{id}')
 - Eliminar Viaje:   Route('/api/viajero/delete/{id}')
 
# 2) compose install
 - Instala la aplicación colocándose en la carpeta raíz del proyecto, y abre la terminal que uses colocando composer install. Ya podrás hacer uso de nuestro Backend.

# 3) doctrine:database:create   
# 3.1) doctrine:migrations:migrate        
   - Antes de iniciar el servidor no te olvides de configurar y crear la base de datos
 
# 4) symfony server:start
   - De igual manera te colocas en el directorio raíz y escribes el comando symfony server:start y tendras el servidor corriendo para hacer uso de las rutas.
       
 
# Frontend Symfony - React:
 - En esta aplicación podrás agregar viajeros a una lista, y a cada uno de estos viajeros podrás agregarle uno o varios viajes, así como consultar, editar, actualizar y eliminar a cada uno de estos(Viajeros - Viajes).
# npm install
 - Instala la aplicación colocándose en la carpeta raíz del proyecto, y abre la terminal que uses colocando npm install. Ya podrás hacer uso de nuestro frontend.

# npm run watch
 - De igual manera te colocas en el directorio raíz y escribes el comando  npm run watch y tendras el servidor corriendo para hacer uso de las rutas.
